" Plugins will be downloaded under the specified directory.
call plug#begin('~/.local/share/nvim/site/plugged')
" Declare the list of plugins.
Plug 'ryanoasis/vim-devicons'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'justinmk/vim-sneak'
Plug 'mattn/emmet-vim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
Plug 'BurntSushi/ripgrep'
Plug 'sharkdp/fd'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-airline/vim-airline-themes'
Plug 'akinsho/toggleterm.nvim', {'tag' : 'v2.*'}
Plug 'jesseduffield/lazygit'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rails'
Plug 'github/copilot.vim'
Plug 'numToStr/Comment.nvim'
Plug 'kyazdani42/nvim-web-devicons' " Recommended (for coloured icons)
" Plug 'ryanoasis/vim-devicons' Icons without colours
Plug 'akinsho/bufferline.nvim', { 'tag': 'v2.*' }
Plug 'thoughtbot/vim-rspec'
" Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'zivyangll/git-blame.vim'
Plug 'vim-airline/vim-airline'
" Visual Settings
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
" Color theme plugins
" List ends here. Plugins become visible to Vim after this call.
call plug#end()
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>
" Start NERDTree and put the cursor back in the other window.
"autocmd VimEnter * NERDTree | wincmd p
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>
" Using Lua functions
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>
" Set defaults
set rnu
set number
set splitbelow
set splitright
  let g:airline#extensions#tabline#buffer_idx_mode = 1
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>0 <Plug>AirlineSelectTab0
nmap <leader>- <Plug>AirlineSelectPrevTab
nmap <leader>+ <Plug>AirlineSelectNextTab
" Close NERDTree on open
let g:NERDTreeQuitOnOpen = 1
let g:airline#extensions#tabline#enabled = 1
" Allow closing of single tab
nnoremap <leader>q :bp<cr>:bd #<cr>
" lua << EOF require("toggleterm").setup{open_mapping = [[<c-\>]],} require('telescope').setup{ defaults = { file_ignore_patterns = {"vendor"} } }
" lua require('toggleterm').setup{open_mapping = [[<c-\>]],}
" lua require('toggleterm').setup{open_mapping = [[<c-n>]],hide_numbers = true,}
lua << EOF
require("toggleterm").setup {
  open_mapping = [[<c-\>]],
  open_mapping = [[<c-n>]],
  terminal_mappings = true,
  hide_numbers = true,
  direction = 'horizontal',
}
function _G.set_terminal_keymaps()
  local opts = {buffer = 0}
  vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
  vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
  vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
  vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
  vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
  vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
end
-- if you only want these mappings for toggle term use term://*toggleterm#* instead
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
EOF
lua << EOF
local Terminal  = require('toggleterm.terminal').Terminal
local lazygit = Terminal:new({
  cmd = "lazygit",
  dir = "git_dir",
  direction = "float",
  -- function to run on opening the terminal
  on_open = function(term)
    vim.cmd("startinsert!")
    vim.api.nvim_buf_set_keymap(term.bufnr, "n", "q", "<cmd>close<CR>", {noremap = true, silent = true})
  end,
  -- function to run on closing the terminal
  on_close = function(term)
    vim.cmd("startinsert!")
  end,
})
function _lazygit_toggle()
  lazygit:toggle()
end
vim.api.nvim_set_keymap("n", "<leader>g", "<cmd>lua _lazygit_toggle()<CR>", {noremap = true, silent = true})
-- local lazygit = Terminal:new({ cmd = "lazygit", hidden = true, direction = 'float', })
--
-- function _lazygit_toggle()
--   lazygit:toggle()
-- end
--
-- vim.api.nvim_set_keymap("n", "<leader>g", "<cmd>lua _lazygit_toggle()<CR>", {noremap = true, silent = true})
vim.api.nvim_set_hl(0, 'FloatBorder', {bg='#000000', fg='#5E81AC'})
vim.api.nvim_set_hl(0, 'NormalFloat', {bg='#000000'})
vim.api.nvim_set_hl(0, 'TelescopeNormal', {bg='#000000'})
vim.api.nvim_set_hl(0, 'TelescopeBorder', {bg='#000000'})
EOF
lua << EOF
require('Comment').setup()
EOF
set termguicolors
lua << EOF
require("bufferline").setup{}
EOF

" main one "
Plug 'ms-jpq/coq_nvim', {'branch': 'coq'}
" 9000+ Snippets"
Plug 'ms-jpq/coq.artifacts', {'branch': 'artifacts'}
" lua & third party sources -- See https://github.com/ms-jpq/coq.thirdparty"
" Need to **configure separately**"
Plug 'ms-jpq/coq.thirdparty', {'branch': '3p'}
" - shell repl "
" - nvim lua api "
" - scientific calculator "
" - comment banner "
" - etc "
" RSpec.vim mappings  "
map <Leader>t :call RunCurrentSpecFile()<CR>
map <Leader>s :call RunNearestSpec()<CR>
map <Leader>l :call RunLastSpec()<CR>
map <Leader>a :call RunAllSpecs()<CR>
let g:rspec_command = "!bundle exec rspec {spec}"
set clipboard=unnamedplus
nnoremap <Leader>s :<C-u>call gitblame#echo()<CR>
syntax on
highlight Visual guifg=white guibg=grey
set clipboard+=unnamedplus
Plug 'ap/vim-css-color'
Plug 'navarasu/onedark.nvim'
call plug#end()

let g:onedark_config = {
  \ 'style': 'deep',
  \ 'toggle_style_key': '<leader>ts',
  \ 'ending_tildes': v:true,
  \ 'diagnostics': {
    \ 'darker': v:false,
    \ 'background': v:false,
  \ },
\ }
colorscheme onedark

